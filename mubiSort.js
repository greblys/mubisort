// ==UserScript==
// @name         Sort MUBI movies by rating
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Sort mubi movies by rating
// @author       You
// @match        https://mubi.com/showing
// @grant        none
// @require http://code.jquery.com/jquery-3.3.1.min.js
// ==/UserScript==

(function() {
    'use strict';
    var promises = [];
    $('head').append('<style>'+
                     '.vjs-loading-spinner {'+
                       'display: block;'+
                       'position: fixed;'+
                       'top: 50%;'+
                       'left: 50%;'+
                       'transition: opacity .2s ease-in-out;'+
                       'opacity: 1;'+
                       'z-index: 9999'+
                     '}'+

                     '.vjs-loading-spinner:after {'+
                       'content: " ";'+
                       'background: url(//assets.mubi.com/assets/mubi-loading-490acd3864d2e0bd52c63df4a5b7ba5e6cbe62f8825e85f2990d94262af273e2.gif) no-repeat;'+
                       'position: absolute;'+
                       'top: -40px;'+
                       'left: -20px;'+
                       'background-size: 80px 80px;'+
                       'width: 81px;'+
                       'height: 81px;'+
                     '}'+
                     '</style>');
    $('body').append('<div class="vjs-loading-spinner"></div>');
    $('.Container:not(.channel-nav__content) article').each(function(){
        var article = this;
        var link = $(this).find('a.full-width-tile__link').attr('href');
        var promise = $.get(link, function(response){
          article.rating = $(response).find('.average-rating__overall').text();
        });
        promises.push(promise);
    });

    $.when.apply($, promises).done(function(){
        $('.Container:not(.channel-nav__content)').html($('.Container:not(.channel-nav__content) article').sort(function(x, y){
            return y.rating - x.rating;
        }));
        $('.vjs-loading-spinner').remove();
    });
})();
